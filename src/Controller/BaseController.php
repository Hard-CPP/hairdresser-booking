<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    /**
     * @Route("/", name="page.calendar")
     */
    public function index()
    {
        return $this->render('calendar/index.html.twig', array(
            'page' => [
                'name' => 'Calendar',
                'description' => '',
                'navbar' => '',
                'header' => true,
                'footer' => 'sticky',
                'class' => '',
                'center' => false,
                'title' => 'Calendar'
            ],
            'config' => [
                'brand' => 'Website',
                'navbar' => 'fixed'
            ]
        ));
    }
}
