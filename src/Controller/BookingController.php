<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use App\Repository\BookingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/booking")
 */
class BookingController extends AbstractController
{
    /**
     * @Route("/", name="page.booking.index", methods={"GET"})
     * @param BookingRepository $bookingRepository
     * @return Response
     */
    public function index(BookingRepository $bookingRepository): Response
    {
        return $this->render('booking/index.html.twig', [
            'bookings' => $bookingRepository->findAll(),
            'page' => [
                'name' => 'Calendar',
                'description' => '',
                'navbar' => '',
                'header' => true,
                'footer' => 'sticky',
                'class' => '',
                'center' => false,
                'title' => 'Calendar'
            ],
            'config' => [
                'brand' => 'Website',
                'navbar' => 'fixed'
            ]
        ]);
    }

    /**
     * @Route("/new", name="page.booking.new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($booking);
            $entityManager->flush();

            return $this->redirectToRoute('page.booking.index');
        }

        return $this->render('booking/new.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
            'page' => [
                'name' => 'Calendar',
                'description' => '',
                'navbar' => '',
                'header' => true,
                'footer' => 'sticky',
                'class' => '',
                'center' => false,
                'title' => 'Calendar'
            ],
            'config' => [
                'brand' => 'Website',
                'navbar' => 'fixed'
            ]
        ]);
    }

    /**
     * @Route("/{id}", name="page.booking.show", methods={"GET"})
     * @param Booking $booking
     * @return Response
     */
    public function show(Booking $booking): Response
    {
        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
            'page' => [
                'name' => 'Calendar',
                'description' => '',
                'navbar' => '',
                'header' => true,
                'footer' => 'sticky',
                'class' => '',
                'center' => false,
                'title' => 'Calendar'
            ],
            'config' => [
                'brand' => 'Website',
                'navbar' => 'fixed'
            ]
        ]);
    }

    /**
     * @Route("/{id}/edit", name="page.booking.edit", methods={"GET","POST"})
     * @param Request $request
     * @param Booking $booking
     * @return Response
     */
    public function edit(Request $request, Booking $booking): Response
    {
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('page.booking.index');
        }

        return $this->render('booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
            'page' => [
                'name' => 'Calendar',
                'description' => '',
                'navbar' => '',
                'header' => true,
                'footer' => 'sticky',
                'class' => '',
                'center' => false,
                'title' => 'Calendar'
            ],
            'config' => [
                'brand' => 'Website',
                'navbar' => 'fixed'
            ]
        ]);
    }

    /**
     * @Route("/{id}", name="booking_delete", methods={"DELETE"})
     * @param Request $request
     * @param Booking $booking
     * @return Response
     */
    public function delete(Request $request, Booking $booking): Response
    {
        if ($this->isCsrfTokenValid('delete'.$booking->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($booking);
            $entityManager->flush();
        }

        return $this->redirectToRoute('page.booking.index', [
            'page' => [
                'name' => 'Calendar',
                'description' => '',
                'navbar' => '',
                'header' => true,
                'footer' => 'sticky',
                'class' => '',
                'center' => false,
                'title' => 'Calendar'
            ],
            'config' => [
                'brand' => 'Website',
                'navbar' => 'fixed'
            ]
        ]);
    }
}
