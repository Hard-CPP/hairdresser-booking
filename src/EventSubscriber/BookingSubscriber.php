<?php

namespace App\EventSubscriber;

use App\Repository\BookingRepository;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BookingSubscriber implements EventSubscriberInterface
{
    private $bookingRepository;
    private $router;

    public function __construct(BookingRepository $bookingRepository, UrlGeneratorInterface $router)
    {
        $this->bookingRepository = $bookingRepository;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return array(CalendarEvents::SET_DATA => 'onCalendarSetData');
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();

        $bookings = $this->bookingRepository
            ->createQueryBuilder('booking')
            ->where('booking.beginAt BETWEEN :start and :end OR booking.endAt BETWEEN :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()->getResult();

        foreach ($bookings as $booking) {
            $bookingEvent = new Event($booking->getTitle(), $booking->getBeginAt(), $booking->getEndAt());
            $bookingEvent->setOptions(array('backgroundColor' => 'blue', 'borderColor' => 'red'));
            $bookingEvent->addOption('url', $this->router->generate('page.booking.show', array('id' => $booking->getId())));
            $calendar->addEvent($bookingEvent);
        }
    }
}